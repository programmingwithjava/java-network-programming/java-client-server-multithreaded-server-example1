import java.io.IOException;
import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) {
       /*
       * port number can be an integer between (0-65535] - 65535 is inclusive
       * some port number will be reserved and some will already be in use.
       * to find it out we assign a port number and see if the socket instance is successfully created.
       * ServerSocket's accept method returns a "socket" instance, not a ServerSocket!
       * this socket will be used to communicate with the client.
       * every client that connects to this server will do so using the same port (5000 in this case),
       * but will use a different socket!
       * */
        try(ServerSocket serverSocket = new ServerSocket(5000)){
            /*
            * this line will not execute until a client is connected to the server
            * on port 5000.
            * our server application will wait until a client gets connected.
            * when a client connects, our server will use input and output streams to receive and send data.
            * socket.getInputStream, socket.getOutputStream will return inout and output streams associated
            * with the server socket instance.
            * a commo practice is to wrap the inputStream with a BufferedReader and wrap the output stream
            * with a PrintWriter.
            *
            * Second argument of the PrintWriter constructor is "true". This sets autoFlust to true.
            * If we don't specify this, then each time we write to outputStream we need to call flush method
            * to make sure that data is sent to the client.
            * */

            /*
            * we want the server to stay alive unti the client does not need it anymore. we can do this by
            * creating an infinite loop as below.
            * */
            /*
            * we are starting a new thread every time server accepts a connection
            * */
            while(true){
                new ClientThread(serverSocket.accept()).start();
                /*
                * the above line is equivalent to this:
                * Socket socket = serverSocket.accept();
                * ClientThread clientThread = new ClientThread(socket);
                * clientThread.start();
                * */
            }
        }
        catch(IOException e){
            System.out.println("Server error: " + e.getMessage());
        }

    }
}
