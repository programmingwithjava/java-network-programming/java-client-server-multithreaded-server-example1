import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientThread extends Thread {
    private Socket socket;

    public ClientThread(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        try{
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(socket.getOutputStream(),true);

            while(true){
                String echoString = input.readLine();
                if(echoString.equals("quit")){
                    break;
                }
                output.println(echoString);
            }
        }
        catch(IOException e){
            System.out.println("Error executing client thread");
        }
        finally{
            try{
                socket.close();
            }
            catch(IOException e){
                System.out.println("Error closing client socket");
            }
        }
    }
}
